Double precision SIMD-oriented Fast Mersenne Twister
====================================================

This package builds the SIMD-oriented Fast Mersenne Twister libraries,
to be used by the offline software of ATLAS.
