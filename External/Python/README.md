Python
======

This package is used to build Python for the offline / analysis release.

While most platforms provide Python natively, the ATLAS software strictly
needs Python 2.7.X to work. Which is not the system default on SLC6 most
notably. So we need to build our own version.

Also on the latest versions of macOS we are unable to use the system
default python version when SIP is enabled. So there we are also
forced to build it ourselves if the user doesn't have a custom build
in their environment already.

The package only builds Python if the `ATLAS_BUILD_PYTHON` (cache)
variable is set to `TRUE`. It's the responsibility of the project
building this "package" to set a correct value for that variable,
allowing the user to override it from the command line if necessary.
