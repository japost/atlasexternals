# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# File describing how to acquire the Intel math libraries for the
# build of an ATLAS project.
#

# Declare the name of the package:
atlas_subdir( MKL )

# The libraries are only available for 64-bit linux. On everything
# else just bail here.
if( NOT "${CMAKE_SYSTEM_NAME}" STREQUAL "Linux" )
   return()
endif()
if( NOT "${CMAKE_SIZEOF_VOID_P}" EQUAL "8" )
   return()
endif()

# Find the MKL libraries:
if( NOT SFT_EXTERNAL )
   if( NOT "$ENV{SFT_EXTERNAL}" STREQUAL "" )
      set( SFT_EXTERNAL $ENV{SFT_EXTERNAL} )
   else()
      set( SFT_EXTERNAL "/cvmfs/projects.cern.ch" )
   endif()
endif()
set( _directory "${SFT_EXTERNAL}/intelsw/psxe/linux/x86_64/2018/compilers_and_libraries_2018.0.128/linux/compiler/lib/intel64_lin" )

# Check if the directory is available:
if( NOT IS_DIRECTORY "${_directory}" )
   message( WARNING "Can't access directory: ${_directory}\n"
      "Intel math libraries are not set up" )
   return()
endif()

# Set up the installation of the libraries:
atlas_install_generic( ${_directory}/*
   DESTINATION lib
   EXECUTABLE )

# Configure the environment setup module:
configure_file(
   ${CMAKE_CURRENT_SOURCE_DIR}/cmake/AtlasMKLEnvironmentConfig.cmake.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/AtlasMKLEnvironmentConfig.cmake
   @ONLY )

# Now set up the environment of the package:
set( AtlasMKLEnvironment_DIR ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   CACHE PATH "Location of AtlasMKLEnvironmentConfig.cmake" )
find_package( AtlasMKLEnvironment )
