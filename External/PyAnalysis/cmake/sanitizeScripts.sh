#!/bin/bash
#
# Script used to sanitize the executable scripts after their installation,
# to make them relocatable.
#

# Fail on errors:
set -e

# Loop over all scripts provided on the command line:
for script in $*; do

    # Create a sanitized version of the script, by just replacing
    # its first line with a relocatable expression:
    sed -i.bak -e '1s:.*:#!/usr/bin/env python:' ${script}
    # Remove the old version:
    rm ${script}.bak
done
